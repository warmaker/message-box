package com.graphzero.messagebox.application.controller;

import com.graphzero.messagebox.domain.Player;
import com.graphzero.messagebox.repository.PlayerRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/messages/")
class MessagesController {
    private final PlayerRepository playerRepository;

    public MessagesController(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @GetMapping("{playerId}")
    public ResponseEntity<Player> getMessages(@PathVariable long playerId) {
        return playerRepository.findByPlayerId(playerId)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.noContent().build());
    }
}
