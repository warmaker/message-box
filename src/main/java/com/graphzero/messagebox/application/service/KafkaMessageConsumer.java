package com.graphzero.messagebox.application.service;

import com.google.gson.Gson;
import com.graphzero.messagebox.domain.Player;
import com.graphzero.messagebox.repository.PlayerRepository;

abstract class KafkaMessageConsumer {
    protected final PlayerRepository playerRepository;
    protected static final Gson gson = new Gson();

    protected KafkaMessageConsumer(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    abstract void consume(String message);

    Player getOrCreatePlayer(long id) {
        return playerRepository
                .findByPlayerId(id)
                .orElseGet(() -> {
                    var player = new Player().playerId(id);
                    return playerRepository.save(player);
                });
    }
}
