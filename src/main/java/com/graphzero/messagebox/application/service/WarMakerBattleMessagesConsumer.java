package com.graphzero.messagebox.application.service;

import com.graphzero.messagebox.application.value.dto.WarPartyBattleDto;
import com.graphzero.messagebox.domain.Player;
import com.graphzero.messagebox.domain.WarPartyBattleMessage;
import com.graphzero.messagebox.repository.HumanUnitsRepository;
import com.graphzero.messagebox.repository.PlayerRepository;
import com.graphzero.messagebox.repository.WarPartyBattleMessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

@Service
class WarMakerBattleMessagesConsumer extends KafkaMessageConsumer {
    private final PlayerRepository playerRepository;
    private final WarPartyBattleMessageRepository warPartyBattleMessageRepository;
    private final HumanUnitsRepository humanUnitsRepository;
    private static final Logger log = LoggerFactory.getLogger(WarMakerBattleMessagesConsumer.class);

    WarMakerBattleMessagesConsumer(PlayerRepository playerRepository,
                                   WarPartyBattleMessageRepository warPartyBattleMessageRepository,
                                   HumanUnitsRepository humanUnitsRepository) {
        super(playerRepository);
        this.playerRepository = playerRepository;
        this.warPartyBattleMessageRepository = warPartyBattleMessageRepository;
        this.humanUnitsRepository = humanUnitsRepository;
    }

    @Override
    void consume(String message) {
        var battleMessage = gson.fromJson(message, WarPartyBattleDto.class);
        playerRepository
                .findByPlayerId(battleMessage.ownerId)
                .ifPresentOrElse(player -> {
                                     var warPartyBattleMessage = getWarPartyBattleMessage(battleMessage);
                                     warPartyBattleMessageRepository.save(warPartyBattleMessage);
                                     player.addBattleMessages(warPartyBattleMessage);
                                     playerRepository.save(player);
                                 },
                                 () -> {
                                     log.debug("Adding new Player with id [{}] to database", battleMessage.ownerId);
                                     var warPartyBattleMessage = getWarPartyBattleMessage(battleMessage);
                                     var player = new Player().playerId(battleMessage.ownerId);
                                     warPartyBattleMessageRepository.save(warPartyBattleMessage);
                                     player.addBattleMessages(warPartyBattleMessage);
                                     playerRepository.save(player);
                                 });
    }

    private WarPartyBattleMessage getWarPartyBattleMessage(WarPartyBattleDto battleMessage) {
        var attackerLoses = battleMessage.battleReport.attackerLoses.toEntity();
        var defenderLoses = battleMessage.battleReport.defenderLoses.toEntity();
        humanUnitsRepository.save(attackerLoses);
        humanUnitsRepository.save(defenderLoses);
        return new WarPartyBattleMessage()
                .attackerWarPartyId(battleMessage.battleReport.attackerWarPartyId)
                .defenderWarPartyId(battleMessage.battleReport.defenderWarPartyId)
                .attackerId(battleMessage.battleReport.attackerPlayerId)
                .defenderId(battleMessage.battleReport.defenderPlayerId)
                .attackerLoses(attackerLoses)
                .defenderLoses(defenderLoses)
                .battleTime(ZonedDateTime.parse(battleMessage.battleReport.battleTime));
    }
}
