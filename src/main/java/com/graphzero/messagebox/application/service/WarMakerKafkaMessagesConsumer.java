package com.graphzero.messagebox.application.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
class WarMakerKafkaMessagesConsumer {
    private final WarMakerBattleMessagesConsumer warMakerBattleMessagesConsumer;
    private final WarMakerWarDeclarationMessagesConsumer warMakerWarDeclarationMessagesConsumer;
    private final Logger log = LoggerFactory.getLogger(WarMakerKafkaMessagesConsumer.class);
    private static final String BATTLE_TOPIC = "battle_topic";
    private static final String WAR_DECLARATION_TOPIC = "war_declaration_topic";

    public WarMakerKafkaMessagesConsumer(WarMakerBattleMessagesConsumer warMakerBattleMessagesConsumer,
                                         WarMakerWarDeclarationMessagesConsumer warMakerWarDeclarationMessagesConsumer) {
        this.warMakerBattleMessagesConsumer = warMakerBattleMessagesConsumer;
        this.warMakerWarDeclarationMessagesConsumer = warMakerWarDeclarationMessagesConsumer;
    }

    @KafkaListener(topics = BATTLE_TOPIC, groupId = "group_id")
    public void consumeBattleMessage(String message) {
        log.info("Consumed battle message [{}] : [{}]", BATTLE_TOPIC, message);
        warMakerBattleMessagesConsumer.consume(message);
    }

    @KafkaListener(topics = WAR_DECLARATION_TOPIC, groupId = "group_id")
    public void consumeWarDeclarationMessage(String message) {
        log.info("Consumed war declaration message [{}] : [{}]", WAR_DECLARATION_TOPIC, message);
        warMakerWarDeclarationMessagesConsumer.consume(message);
    }
}
