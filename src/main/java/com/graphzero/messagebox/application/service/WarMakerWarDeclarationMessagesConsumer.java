package com.graphzero.messagebox.application.service;

import com.graphzero.messagebox.application.value.dto.WarDeclaredEventDto;
import com.graphzero.messagebox.domain.PlayerWarDeclarationMessage;
import com.graphzero.messagebox.repository.PlayerRepository;
import com.graphzero.messagebox.repository.PlayerWarDeclarationMessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
class WarMakerWarDeclarationMessagesConsumer extends KafkaMessageConsumer {
    private final PlayerWarDeclarationMessageRepository playerWarDeclarationMessageRepository;
    private static final Logger log = LoggerFactory.getLogger(WarMakerWarDeclarationMessagesConsumer.class);

    WarMakerWarDeclarationMessagesConsumer(PlayerRepository playerRepository,
                                           PlayerWarDeclarationMessageRepository playerWarDeclarationMessageRepository) {
        super(playerRepository);
        this.playerWarDeclarationMessageRepository = playerWarDeclarationMessageRepository;
    }

    @Override
    void consume(String message) {
        var warDeclarationMessage = gson.fromJson(message, WarDeclaredEventDto.class);
        var attackingPlayer = getOrCreatePlayer(warDeclarationMessage.attackerId);
        var defendingPlayer = getOrCreatePlayer(warDeclarationMessage.defenderId);
        var warDeclaration = new PlayerWarDeclarationMessage()
                .attacker(attackingPlayer)
                .defender(defendingPlayer)
                .declarationTime(warDeclarationMessage.getDeclarationTime());
        attackingPlayer.addAttackingWarDeclarationMessages(warDeclaration);
        defendingPlayer.addDefendingWarDeclarationMessages(warDeclaration);
        playerWarDeclarationMessageRepository.save(warDeclaration);
        playerRepository.save(attackingPlayer);
        playerRepository.save(defendingPlayer);
    }

}
