package com.graphzero.messagebox.application.value.dto;


public class BattleReport {
    public final String battleTime;
    public final BattleResult battleResult;
    public final HumanUnitsListDto attackerLoses;
    public final HumanUnitsListDto defenderLoses;
    public final long attackerWarPartyId;
    public final long defenderWarPartyId;
    public final long attackerPlayerId;
    public final long defenderPlayerId;

    public BattleReport(String battleTime, BattleResult battleResult,
                        HumanUnitsListDto attackerLoses,
                        HumanUnitsListDto defenderLoses,
                        long attackerWarPartyId, long defenderWarPartyId,
                        long attackerPlayerId, long defenderPlayerId) {
        this.battleTime = battleTime;
        this.battleResult = battleResult;
        this.attackerLoses = attackerLoses;
        this.defenderLoses = defenderLoses;
        this.attackerWarPartyId = attackerWarPartyId;
        this.defenderWarPartyId = defenderWarPartyId;
        this.attackerPlayerId = attackerPlayerId;
        this.defenderPlayerId = defenderPlayerId;
    }
}
