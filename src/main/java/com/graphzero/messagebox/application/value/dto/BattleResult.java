package com.graphzero.messagebox.application.value.dto;

enum BattleResult {
    ATTACKER_WINS,
    DEFENDER_WINS,
    DRAW
}
