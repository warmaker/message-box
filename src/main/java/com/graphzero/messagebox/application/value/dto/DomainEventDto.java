package com.graphzero.messagebox.application.value.dto;

import java.io.Serializable;
import java.util.UUID;

abstract class DomainEventDto implements Serializable {
    public final UUID uuid;

    public DomainEventDto(UUID uuid) {
        this.uuid = uuid;
    }
}
