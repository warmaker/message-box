package com.graphzero.messagebox.application.value.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.graphzero.messagebox.domain.HumanUnits;

import java.util.Objects;

public class HumanUnitsListDto {
    public int spearman;
    public int swordsman;
    public int archers;
    public int crossbowman;

    public HumanUnitsListDto(@JsonProperty("spearman") int spearman,
                             @JsonProperty("swordsman") int swordsman,
                             @JsonProperty("archers") int archers,
                             @JsonProperty("crossbowman") int crossbowman) {
        this.spearman = spearman;
        this.swordsman = swordsman;
        this.archers = archers;
        this.crossbowman = crossbowman;
    }

    public HumanUnits toEntity() {
        return new HumanUnits().spearman(spearman).swordsman(swordsman).archers(archers).crossbowman(crossbowman);
    }

    @Override
    public String toString() {
        return "HumanUnitsListDto{" +
            "spearman=" + spearman +
            ", swordsman=" + swordsman +
            ", archers=" + archers +
            ", crossbowman=" + crossbowman +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HumanUnitsListDto)) return false;
        HumanUnitsListDto that = (HumanUnitsListDto) o;
        return spearman == that.spearman &&
            swordsman == that.swordsman &&
            archers == that.archers &&
            crossbowman == that.crossbowman;
    }

    @Override
    public int hashCode() {
        return Objects.hash(spearman, swordsman, archers, crossbowman);
    }
}
