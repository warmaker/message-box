package com.graphzero.messagebox.application.value.dto;

import java.time.ZonedDateTime;
import java.util.UUID;

public class WarDeclaredEventDto extends DomainEventDto {
    public final long attackerId;
    public final long defenderId;
    public final String declarationTime;
    public final WarMakerPlayerEvent type = WarMakerPlayerEvent.WAR_DECLARED;

    public WarDeclaredEventDto(UUID uuid,
                               long attackerId,
                               long defenderId,
                               String declarationTime) {
        super(uuid);
        this.attackerId = attackerId;
        this.defenderId = defenderId;
        this.declarationTime = declarationTime;
    }

    public ZonedDateTime getDeclarationTime() {
        return ZonedDateTime.parse(declarationTime);
    }
}
