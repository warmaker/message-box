package com.graphzero.messagebox.application.value.dto;

enum WarMakerArmyEvent implements WarMakerEventType {
    WAR_PARTY_BATTLES
}
