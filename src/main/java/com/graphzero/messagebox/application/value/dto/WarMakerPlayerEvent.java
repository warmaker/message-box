package com.graphzero.messagebox.application.value.dto;

enum WarMakerPlayerEvent implements WarMakerEventType {
    WAR_DECLARED
}
