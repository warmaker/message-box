package com.graphzero.messagebox.application.value.dto;

import java.util.UUID;

public class WarPartyBattleDto extends DomainEventDto {
    public final long warPartyId;
    public final long ownerId;
    public final BattleReport battleReport;
    public final WarMakerArmyEvent type = WarMakerArmyEvent.WAR_PARTY_BATTLES;

    public WarPartyBattleDto(UUID uuid,
                             long warPartyId,
                             long ownerId,
                             BattleReport battleReport) {
        super(uuid);
        this.warPartyId = warPartyId;
        this.ownerId = ownerId;
        this.battleReport = battleReport;
    }
}
