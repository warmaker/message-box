package com.graphzero.messagebox.config.dbmigrations;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;

@ChangeLog
public class AddPlayerAndBattleMessage {

    @ChangeSet(order = "001", id = "addPlayerAndBattleMessage", author = "GraphZero")
    public void importantWorkToDo(DB db) {
        db.getCollection("player").insert(new BasicDBObject());
        db.getCollection("war_party_battle_message").insert(new BasicDBObject());
    }

}
