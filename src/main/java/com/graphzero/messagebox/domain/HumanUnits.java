package com.graphzero.messagebox.domain;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * A HumanUnits.
 */
@Document(collection = "human_units")
public class HumanUnits implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("spearman")
    private Integer spearman;

    @Field("swordsman")
    private Integer swordsman;

    @Field("archers")
    private Integer archers;

    @Field("crossbowman")
    private Integer crossbowman;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getSpearman() {
        return spearman;
    }

    public HumanUnits spearman(Integer spearman) {
        this.spearman = spearman;
        return this;
    }

    public void setSpearman(Integer spearman) {
        this.spearman = spearman;
    }

    public Integer getSwordsman() {
        return swordsman;
    }

    public HumanUnits swordsman(Integer swordsman) {
        this.swordsman = swordsman;
        return this;
    }

    public void setSwordsman(Integer swordsman) {
        this.swordsman = swordsman;
    }

    public Integer getArchers() {
        return archers;
    }

    public HumanUnits archers(Integer archers) {
        this.archers = archers;
        return this;
    }

    public void setArchers(Integer archers) {
        this.archers = archers;
    }

    public Integer getCrossbowman() {
        return crossbowman;
    }

    public HumanUnits crossbowman(Integer crossbowman) {
        this.crossbowman = crossbowman;
        return this;
    }

    public void setCrossbowman(Integer crossbowman) {
        this.crossbowman = crossbowman;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HumanUnits)) {
            return false;
        }
        return id != null && id.equals(((HumanUnits) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "HumanUnits{" +
            "id=" + getId() +
            ", spearman=" + getSpearman() +
            ", swordsman=" + getSwordsman() +
            ", archers=" + getArchers() +
            ", crossbowman=" + getCrossbowman() +
            "}";
    }
}
