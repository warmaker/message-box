package com.graphzero.messagebox.domain;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Player.
 */
@Document(collection = "player")
public class Player implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("player_id")
    private Long playerId;

    @DBRef
    @Field("battleMessages")
    private Set<WarPartyBattleMessage> battleMessages = new HashSet<>();

    @DBRef
    @Field("attackingWarDeclarationMessages")
    private Set<PlayerWarDeclarationMessage> attackingWarDeclarationMessages = new HashSet<>();

    @DBRef
    @Field("defendingWarDeclarationMessages")
    private Set<PlayerWarDeclarationMessage> defendingWarDeclarationMessages = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public Player playerId(Long playerId) {
        this.playerId = playerId;
        return this;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public Set<WarPartyBattleMessage> getBattleMessages() {
        return battleMessages;
    }

    public Player battleMessages(Set<WarPartyBattleMessage> warPartyBattleMessages) {
        this.battleMessages = warPartyBattleMessages;
        return this;
    }

    public Player addBattleMessages(WarPartyBattleMessage warPartyBattleMessage) {
        this.battleMessages.add(warPartyBattleMessage);
        warPartyBattleMessage.setPlayer(this);
        return this;
    }

    public Player removeBattleMessages(WarPartyBattleMessage warPartyBattleMessage) {
        this.battleMessages.remove(warPartyBattleMessage);
        warPartyBattleMessage.setPlayer(null);
        return this;
    }

    public void setBattleMessages(Set<WarPartyBattleMessage> warPartyBattleMessages) {
        this.battleMessages = warPartyBattleMessages;
    }

    public Set<PlayerWarDeclarationMessage> getAttackingWarDeclarationMessages() {
        return attackingWarDeclarationMessages;
    }

    public Player attackingWarDeclarationMessages(Set<PlayerWarDeclarationMessage> playerWarDeclarationMessages) {
        this.attackingWarDeclarationMessages = playerWarDeclarationMessages;
        return this;
    }

    public Player addAttackingWarDeclarationMessages(PlayerWarDeclarationMessage playerWarDeclarationMessage) {
        this.attackingWarDeclarationMessages.add(playerWarDeclarationMessage);
        playerWarDeclarationMessage.setAttacker(this);
        return this;
    }

    public Player removeAttackingWarDeclarationMessages(PlayerWarDeclarationMessage playerWarDeclarationMessage) {
        this.attackingWarDeclarationMessages.remove(playerWarDeclarationMessage);
        playerWarDeclarationMessage.setAttacker(null);
        return this;
    }

    public void setAttackingWarDeclarationMessages(Set<PlayerWarDeclarationMessage> playerWarDeclarationMessages) {
        this.attackingWarDeclarationMessages = playerWarDeclarationMessages;
    }

    public Set<PlayerWarDeclarationMessage> getDefendingWarDeclarationMessages() {
        return defendingWarDeclarationMessages;
    }

    public Player defendingWarDeclarationMessages(Set<PlayerWarDeclarationMessage> playerWarDeclarationMessages) {
        this.defendingWarDeclarationMessages = playerWarDeclarationMessages;
        return this;
    }

    public Player addDefendingWarDeclarationMessages(PlayerWarDeclarationMessage playerWarDeclarationMessage) {
        this.defendingWarDeclarationMessages.add(playerWarDeclarationMessage);
        playerWarDeclarationMessage.setDefender(this);
        return this;
    }

    public Player removeDefendingWarDeclarationMessages(PlayerWarDeclarationMessage playerWarDeclarationMessage) {
        this.defendingWarDeclarationMessages.remove(playerWarDeclarationMessage);
        playerWarDeclarationMessage.setDefender(null);
        return this;
    }

    public void setDefendingWarDeclarationMessages(Set<PlayerWarDeclarationMessage> playerWarDeclarationMessages) {
        this.defendingWarDeclarationMessages = playerWarDeclarationMessages;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Player)) {
            return false;
        }
        return id != null && id.equals(((Player) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Player{" +
            "id=" + getId() +
            ", playerId=" + getPlayerId() +
            "}";
    }
}
