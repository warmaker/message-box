package com.graphzero.messagebox.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A PlayerWarDeclarationMessage.
 */
@Document(collection = "player_war_declaration_message")
public class PlayerWarDeclarationMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("declaration_time")
    private ZonedDateTime declarationTime;

    @DBRef
    @Field("attacker")
    @JsonIgnoreProperties({"attackingWarDeclarationMessages", "defendingWarDeclarationMessages"})
    private Player attacker;

    @DBRef
    @Field("defender")
    @JsonIgnoreProperties({"attackingWarDeclarationMessages", "defendingWarDeclarationMessages"})
    private Player defender;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ZonedDateTime getDeclarationTime() {
        return declarationTime;
    }

    public PlayerWarDeclarationMessage declarationTime(ZonedDateTime declarationTime) {
        this.declarationTime = declarationTime;
        return this;
    }

    public void setDeclarationTime(ZonedDateTime declarationTime) {
        this.declarationTime = declarationTime;
    }

    public Player getAttacker() {
        return attacker;
    }

    public PlayerWarDeclarationMessage attacker(Player player) {
        this.attacker = player;
        return this;
    }

    public void setAttacker(Player player) {
        this.attacker = player;
    }

    public Player getDefender() {
        return defender;
    }

    public PlayerWarDeclarationMessage defender(Player player) {
        this.defender = player;
        return this;
    }

    public void setDefender(Player player) {
        this.defender = player;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PlayerWarDeclarationMessage)) {
            return false;
        }
        return id != null && id.equals(((PlayerWarDeclarationMessage) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "PlayerWarDeclarationMessage{" +
            "id=" + getId() +
            ", declarationTime='" + getDeclarationTime() + "'" +
            "}";
    }
}
