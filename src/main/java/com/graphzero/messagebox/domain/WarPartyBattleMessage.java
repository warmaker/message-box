package com.graphzero.messagebox.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A WarPartyBattleMessage.
 */
@Document(collection = "war_party_battle_message")
public class WarPartyBattleMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("battle_time")
    private ZonedDateTime battleTime;

    @Field("attacker_war_party_id")
    private Long attackerWarPartyId;

    @Field("defender_war_party_id")
    private Long defenderWarPartyId;

    @Field("attacker_id")
    private Long attackerId;

    @Field("defender_id")
    private Long defenderId;

    @DBRef
    @Field("player")
    @JsonIgnoreProperties("warPartyBattleMessages")
    private Player player;

    @DBRef
    @Field("attackerLoses")
    private HumanUnits attackerLoses;

    @DBRef
    @Field("defenderLoses")
    private HumanUnits defenderLoses;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ZonedDateTime getBattleTime() {
        return battleTime;
    }

    public WarPartyBattleMessage battleTime(ZonedDateTime battleTime) {
        this.battleTime = battleTime;
        return this;
    }

    public void setBattleTime(ZonedDateTime battleTime) {
        this.battleTime = battleTime;
    }

    public Long getAttackerWarPartyId() {
        return attackerWarPartyId;
    }

    public WarPartyBattleMessage attackerWarPartyId(Long attackerWarPartyId) {
        this.attackerWarPartyId = attackerWarPartyId;
        return this;
    }

    public void setAttackerWarPartyId(Long attackerWarPartyId) {
        this.attackerWarPartyId = attackerWarPartyId;
    }

    public Long getDefenderWarPartyId() {
        return defenderWarPartyId;
    }

    public WarPartyBattleMessage defenderWarPartyId(Long defenderWarPartyId) {
        this.defenderWarPartyId = defenderWarPartyId;
        return this;
    }

    public void setDefenderWarPartyId(Long defenderWarPartyId) {
        this.defenderWarPartyId = defenderWarPartyId;
    }

    public Long getAttackerId() {
        return attackerId;
    }

    public WarPartyBattleMessage attackerId(Long attackerId) {
        this.attackerId = attackerId;
        return this;
    }

    public void setAttackerId(Long attackerId) {
        this.attackerId = attackerId;
    }

    public Long getDefenderId() {
        return defenderId;
    }

    public WarPartyBattleMessage defenderId(Long defenderId) {
        this.defenderId = defenderId;
        return this;
    }

    public void setDefenderId(Long defenderId) {
        this.defenderId = defenderId;
    }

    public Player getPlayer() {
        return player;
    }

    public WarPartyBattleMessage player(Player player) {
        this.player = player;
        return this;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public HumanUnits getAttackerLoses() {
        return attackerLoses;
    }

    public WarPartyBattleMessage attackerLoses(HumanUnits humanUnits) {
        this.attackerLoses = humanUnits;
        return this;
    }

    public void setAttackerLoses(HumanUnits humanUnits) {
        this.attackerLoses = humanUnits;
    }

    public HumanUnits getDefenderLoses() {
        return defenderLoses;
    }

    public WarPartyBattleMessage defenderLoses(HumanUnits humanUnits) {
        this.defenderLoses = humanUnits;
        return this;
    }

    public void setDefenderLoses(HumanUnits humanUnits) {
        this.defenderLoses = humanUnits;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof WarPartyBattleMessage)) {
            return false;
        }
        return id != null && id.equals(((WarPartyBattleMessage) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "WarPartyBattleMessage{" +
            "id=" + getId() +
            ", battleTime='" + getBattleTime() + "'" +
            ", attackerWarPartyId=" + getAttackerWarPartyId() +
            ", defenderWarPartyId=" + getDefenderWarPartyId() +
            ", attackerId=" + getAttackerId() +
            ", defenderId=" + getDefenderId() +
            "}";
    }
}
