package com.graphzero.messagebox.repository;

import com.graphzero.messagebox.domain.HumanUnits;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data MongoDB repository for the HumanUnits entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HumanUnitsRepository extends MongoRepository<HumanUnits, String> {

}
