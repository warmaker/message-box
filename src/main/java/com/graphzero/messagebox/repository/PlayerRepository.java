package com.graphzero.messagebox.repository;

import com.graphzero.messagebox.domain.Player;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data MongoDB repository for the Player entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlayerRepository extends MongoRepository<Player, String> {
    Optional<Player> findByPlayerId(long id);
}
