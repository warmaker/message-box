package com.graphzero.messagebox.repository;

import com.graphzero.messagebox.domain.PlayerWarDeclarationMessage;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data MongoDB repository for the PlayerWarDeclarationMessage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlayerWarDeclarationMessageRepository extends MongoRepository<PlayerWarDeclarationMessage, String> {

}
