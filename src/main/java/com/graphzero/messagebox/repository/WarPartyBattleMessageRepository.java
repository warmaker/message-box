package com.graphzero.messagebox.repository;

import com.graphzero.messagebox.domain.WarPartyBattleMessage;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data MongoDB repository for the WarPartyBattleMessage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WarPartyBattleMessageRepository extends MongoRepository<WarPartyBattleMessage, String> {

}
