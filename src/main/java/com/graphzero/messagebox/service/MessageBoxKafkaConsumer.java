package com.graphzero.messagebox.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class MessageBoxKafkaConsumer {

    private final Logger log = LoggerFactory.getLogger(MessageBoxKafkaConsumer.class);
    private static final String TOPIC = "topic_messagebox";

    @KafkaListener(topics = "topic_messagebox", groupId = "group_id")
    public void consume(String message) throws IOException {
        log.info("Consumed message in {} : {}", TOPIC, message);
    }
}
