package com.graphzero.messagebox.web.rest;

import com.graphzero.messagebox.domain.HumanUnits;
import com.graphzero.messagebox.repository.HumanUnitsRepository;
import com.graphzero.messagebox.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.graphzero.messagebox.domain.HumanUnits}.
 */
@RestController
@RequestMapping("/api")
public class HumanUnitsResource {

    private final Logger log = LoggerFactory.getLogger(HumanUnitsResource.class);

    private static final String ENTITY_NAME = "messageBoxHumanUnits";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HumanUnitsRepository humanUnitsRepository;

    public HumanUnitsResource(HumanUnitsRepository humanUnitsRepository) {
        this.humanUnitsRepository = humanUnitsRepository;
    }

    /**
     * {@code POST  /human-units} : Create a new humanUnits.
     *
     * @param humanUnits the humanUnits to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new humanUnits, or with status {@code 400 (Bad Request)} if the humanUnits has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/human-units")
    public ResponseEntity<HumanUnits> createHumanUnits(@RequestBody HumanUnits humanUnits) throws URISyntaxException {
        log.debug("REST request to save HumanUnits : {}", humanUnits);
        if (humanUnits.getId() != null) {
            throw new BadRequestAlertException("A new humanUnits cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HumanUnits result = humanUnitsRepository.save(humanUnits);
        return ResponseEntity.created(new URI("/api/human-units/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /human-units} : Updates an existing humanUnits.
     *
     * @param humanUnits the humanUnits to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated humanUnits,
     * or with status {@code 400 (Bad Request)} if the humanUnits is not valid,
     * or with status {@code 500 (Internal Server Error)} if the humanUnits couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/human-units")
    public ResponseEntity<HumanUnits> updateHumanUnits(@RequestBody HumanUnits humanUnits) throws URISyntaxException {
        log.debug("REST request to update HumanUnits : {}", humanUnits);
        if (humanUnits.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        HumanUnits result = humanUnitsRepository.save(humanUnits);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, humanUnits.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /human-units} : get all the humanUnits.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of humanUnits in body.
     */
    @GetMapping("/human-units")
    public List<HumanUnits> getAllHumanUnits() {
        log.debug("REST request to get all HumanUnits");
        return humanUnitsRepository.findAll();
    }

    /**
     * {@code GET  /human-units/:id} : get the "id" humanUnits.
     *
     * @param id the id of the humanUnits to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the humanUnits, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/human-units/{id}")
    public ResponseEntity<HumanUnits> getHumanUnits(@PathVariable String id) {
        log.debug("REST request to get HumanUnits : {}", id);
        Optional<HumanUnits> humanUnits = humanUnitsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(humanUnits);
    }

    /**
     * {@code DELETE  /human-units/:id} : delete the "id" humanUnits.
     *
     * @param id the id of the humanUnits to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/human-units/{id}")
    public ResponseEntity<Void> deleteHumanUnits(@PathVariable String id) {
        log.debug("REST request to delete HumanUnits : {}", id);
        humanUnitsRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
