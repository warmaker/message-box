package com.graphzero.messagebox.web.rest;

import com.graphzero.messagebox.service.MessageBoxKafkaProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/message-box-kafka")
public class MessageBoxKafkaResource {

    private final Logger log = LoggerFactory.getLogger(MessageBoxKafkaResource.class);

    private MessageBoxKafkaProducer kafkaProducer;

    public MessageBoxKafkaResource(MessageBoxKafkaProducer kafkaProducer) {
        this.kafkaProducer = kafkaProducer;
    }

    @PostMapping(value = "/publish")
    public void sendMessageToKafkaTopic(@RequestParam("message") String message) {
        log.debug("REST request to send to Kafka topic the message : {}", message);
        this.kafkaProducer.sendMessage(message);
    }
}
