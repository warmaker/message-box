package com.graphzero.messagebox.web.rest;

import com.graphzero.messagebox.domain.PlayerWarDeclarationMessage;
import com.graphzero.messagebox.repository.PlayerWarDeclarationMessageRepository;
import com.graphzero.messagebox.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.graphzero.messagebox.domain.PlayerWarDeclarationMessage}.
 */
@RestController
@RequestMapping("/api")
public class PlayerWarDeclarationMessageResource {

    private final Logger log = LoggerFactory.getLogger(PlayerWarDeclarationMessageResource.class);

    private static final String ENTITY_NAME = "messageBoxPlayerWarDeclarationMessage";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PlayerWarDeclarationMessageRepository playerWarDeclarationMessageRepository;

    public PlayerWarDeclarationMessageResource(PlayerWarDeclarationMessageRepository playerWarDeclarationMessageRepository) {
        this.playerWarDeclarationMessageRepository = playerWarDeclarationMessageRepository;
    }

    /**
     * {@code POST  /player-war-declaration-messages} : Create a new playerWarDeclarationMessage.
     *
     * @param playerWarDeclarationMessage the playerWarDeclarationMessage to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new playerWarDeclarationMessage, or with status {@code 400 (Bad Request)} if the playerWarDeclarationMessage has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/player-war-declaration-messages")
    public ResponseEntity<PlayerWarDeclarationMessage> createPlayerWarDeclarationMessage(@RequestBody PlayerWarDeclarationMessage playerWarDeclarationMessage) throws URISyntaxException {
        log.debug("REST request to save PlayerWarDeclarationMessage : {}", playerWarDeclarationMessage);
        if (playerWarDeclarationMessage.getId() != null) {
            throw new BadRequestAlertException("A new playerWarDeclarationMessage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlayerWarDeclarationMessage result = playerWarDeclarationMessageRepository.save(playerWarDeclarationMessage);
        return ResponseEntity.created(new URI("/api/player-war-declaration-messages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /player-war-declaration-messages} : Updates an existing playerWarDeclarationMessage.
     *
     * @param playerWarDeclarationMessage the playerWarDeclarationMessage to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated playerWarDeclarationMessage,
     * or with status {@code 400 (Bad Request)} if the playerWarDeclarationMessage is not valid,
     * or with status {@code 500 (Internal Server Error)} if the playerWarDeclarationMessage couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/player-war-declaration-messages")
    public ResponseEntity<PlayerWarDeclarationMessage> updatePlayerWarDeclarationMessage(@RequestBody PlayerWarDeclarationMessage playerWarDeclarationMessage) throws URISyntaxException {
        log.debug("REST request to update PlayerWarDeclarationMessage : {}", playerWarDeclarationMessage);
        if (playerWarDeclarationMessage.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PlayerWarDeclarationMessage result = playerWarDeclarationMessageRepository.save(playerWarDeclarationMessage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, playerWarDeclarationMessage.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /player-war-declaration-messages} : get all the playerWarDeclarationMessages.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of playerWarDeclarationMessages in body.
     */
    @GetMapping("/player-war-declaration-messages")
    public List<PlayerWarDeclarationMessage> getAllPlayerWarDeclarationMessages() {
        log.debug("REST request to get all PlayerWarDeclarationMessages");
        return playerWarDeclarationMessageRepository.findAll();
    }

    /**
     * {@code GET  /player-war-declaration-messages/:id} : get the "id" playerWarDeclarationMessage.
     *
     * @param id the id of the playerWarDeclarationMessage to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the playerWarDeclarationMessage, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/player-war-declaration-messages/{id}")
    public ResponseEntity<PlayerWarDeclarationMessage> getPlayerWarDeclarationMessage(@PathVariable String id) {
        log.debug("REST request to get PlayerWarDeclarationMessage : {}", id);
        Optional<PlayerWarDeclarationMessage> playerWarDeclarationMessage = playerWarDeclarationMessageRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(playerWarDeclarationMessage);
    }

    /**
     * {@code DELETE  /player-war-declaration-messages/:id} : delete the "id" playerWarDeclarationMessage.
     *
     * @param id the id of the playerWarDeclarationMessage to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/player-war-declaration-messages/{id}")
    public ResponseEntity<Void> deletePlayerWarDeclarationMessage(@PathVariable String id) {
        log.debug("REST request to delete PlayerWarDeclarationMessage : {}", id);
        playerWarDeclarationMessageRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
