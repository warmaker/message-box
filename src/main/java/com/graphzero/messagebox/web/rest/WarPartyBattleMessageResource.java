package com.graphzero.messagebox.web.rest;

import com.graphzero.messagebox.domain.WarPartyBattleMessage;
import com.graphzero.messagebox.repository.WarPartyBattleMessageRepository;
import com.graphzero.messagebox.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.graphzero.messagebox.domain.WarPartyBattleMessage}.
 */
@RestController
@RequestMapping("/api")
public class WarPartyBattleMessageResource {

    private final Logger log = LoggerFactory.getLogger(WarPartyBattleMessageResource.class);

    private static final String ENTITY_NAME = "messageBoxWarPartyBattleMessage";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WarPartyBattleMessageRepository warPartyBattleMessageRepository;

    public WarPartyBattleMessageResource(WarPartyBattleMessageRepository warPartyBattleMessageRepository) {
        this.warPartyBattleMessageRepository = warPartyBattleMessageRepository;
    }

    /**
     * {@code POST  /war-party-battle-messages} : Create a new warPartyBattleMessage.
     *
     * @param warPartyBattleMessage the warPartyBattleMessage to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new warPartyBattleMessage, or with status {@code 400 (Bad Request)} if the warPartyBattleMessage has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/war-party-battle-messages")
    public ResponseEntity<WarPartyBattleMessage> createWarPartyBattleMessage(@RequestBody WarPartyBattleMessage warPartyBattleMessage) throws URISyntaxException {
        log.debug("REST request to save WarPartyBattleMessage : {}", warPartyBattleMessage);
        if (warPartyBattleMessage.getId() != null) {
            throw new BadRequestAlertException("A new warPartyBattleMessage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WarPartyBattleMessage result = warPartyBattleMessageRepository.save(warPartyBattleMessage);
        return ResponseEntity.created(new URI("/api/war-party-battle-messages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /war-party-battle-messages} : Updates an existing warPartyBattleMessage.
     *
     * @param warPartyBattleMessage the warPartyBattleMessage to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated warPartyBattleMessage,
     * or with status {@code 400 (Bad Request)} if the warPartyBattleMessage is not valid,
     * or with status {@code 500 (Internal Server Error)} if the warPartyBattleMessage couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/war-party-battle-messages")
    public ResponseEntity<WarPartyBattleMessage> updateWarPartyBattleMessage(@RequestBody WarPartyBattleMessage warPartyBattleMessage) throws URISyntaxException {
        log.debug("REST request to update WarPartyBattleMessage : {}", warPartyBattleMessage);
        if (warPartyBattleMessage.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WarPartyBattleMessage result = warPartyBattleMessageRepository.save(warPartyBattleMessage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, warPartyBattleMessage.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /war-party-battle-messages} : get all the warPartyBattleMessages.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of warPartyBattleMessages in body.
     */
    @GetMapping("/war-party-battle-messages")
    public List<WarPartyBattleMessage> getAllWarPartyBattleMessages() {
        log.debug("REST request to get all WarPartyBattleMessages");
        return warPartyBattleMessageRepository.findAll();
    }

    /**
     * {@code GET  /war-party-battle-messages/:id} : get the "id" warPartyBattleMessage.
     *
     * @param id the id of the warPartyBattleMessage to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the warPartyBattleMessage, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/war-party-battle-messages/{id}")
    public ResponseEntity<WarPartyBattleMessage> getWarPartyBattleMessage(@PathVariable String id) {
        log.debug("REST request to get WarPartyBattleMessage : {}", id);
        Optional<WarPartyBattleMessage> warPartyBattleMessage = warPartyBattleMessageRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(warPartyBattleMessage);
    }

    /**
     * {@code DELETE  /war-party-battle-messages/:id} : delete the "id" warPartyBattleMessage.
     *
     * @param id the id of the warPartyBattleMessage to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/war-party-battle-messages/{id}")
    public ResponseEntity<Void> deleteWarPartyBattleMessage(@PathVariable String id) {
        log.debug("REST request to delete WarPartyBattleMessage : {}", id);
        warPartyBattleMessageRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
