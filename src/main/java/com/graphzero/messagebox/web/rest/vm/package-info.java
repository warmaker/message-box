/**
 * View Models used by Spring MVC REST controllers.
 */
package com.graphzero.messagebox.web.rest.vm;
