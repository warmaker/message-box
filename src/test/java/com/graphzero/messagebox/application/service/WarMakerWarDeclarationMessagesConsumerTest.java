package com.graphzero.messagebox.application.service;

import com.google.gson.Gson;
import com.graphzero.messagebox.MessageBoxApp;
import com.graphzero.messagebox.application.value.dto.WarDeclaredEventDto;
import com.graphzero.messagebox.domain.Player;
import com.graphzero.messagebox.repository.PlayerRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;

import java.time.ZonedDateTime;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;


@EmbeddedKafka
@SpringBootTest(classes = MessageBoxApp.class)
class WarMakerWarDeclarationMessagesConsumerTest {
    protected static final Gson gson = new Gson();

    @Autowired
    WarMakerWarDeclarationMessagesConsumer warMakerWarDeclarationMessagesConsumer;

    @Autowired
    PlayerRepository playerRepository;

    @Test
    @DisplayName("")
    void name() {
        // given
        var attackerId = 1L;
        var defenderId = 2L;
        var message = gson.toJson(new WarDeclaredEventDto(UUID.randomUUID(), attackerId, defenderId, ZonedDateTime.now().toString()));
        // when
        warMakerWarDeclarationMessagesConsumer.consume(message);
        // then
        assertThat(playerRepository.findAll()).hasSize(3);
        var attacker = playerRepository.findAll().stream().filter(it -> it.getPlayerId().equals(attackerId)).findFirst()
                .get();
        var defender = playerRepository.findAll().stream().filter(it -> it.getPlayerId().equals(defenderId)).findFirst()
                .get();

        assertThat(attacker.getAttackingWarDeclarationMessages()).hasSize(1).first().matches(it -> it.getDefender().equals(defender));
    }
}