package com.graphzero.messagebox.web.rest;

import com.graphzero.messagebox.MessageBoxApp;
import com.graphzero.messagebox.domain.HumanUnits;
import com.graphzero.messagebox.repository.HumanUnitsRepository;
import com.graphzero.messagebox.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;


import java.util.List;

import static com.graphzero.messagebox.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link HumanUnitsResource} REST controller.
 */
@EmbeddedKafka
@SpringBootTest(classes = MessageBoxApp.class)
public class HumanUnitsResourceIT {

    private static final Integer DEFAULT_SPEARMAN = 1;
    private static final Integer UPDATED_SPEARMAN = 2;
    private static final Integer SMALLER_SPEARMAN = 1 - 1;

    private static final Integer DEFAULT_SWORDSMAN = 1;
    private static final Integer UPDATED_SWORDSMAN = 2;
    private static final Integer SMALLER_SWORDSMAN = 1 - 1;

    private static final Integer DEFAULT_ARCHERS = 1;
    private static final Integer UPDATED_ARCHERS = 2;
    private static final Integer SMALLER_ARCHERS = 1 - 1;

    private static final Integer DEFAULT_CROSSBOWMAN = 1;
    private static final Integer UPDATED_CROSSBOWMAN = 2;
    private static final Integer SMALLER_CROSSBOWMAN = 1 - 1;

    @Autowired
    private HumanUnitsRepository humanUnitsRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    private MockMvc restHumanUnitsMockMvc;

    private HumanUnits humanUnits;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final HumanUnitsResource humanUnitsResource = new HumanUnitsResource(humanUnitsRepository);
        this.restHumanUnitsMockMvc = MockMvcBuilders.standaloneSetup(humanUnitsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HumanUnits createEntity() {
        HumanUnits humanUnits = new HumanUnits()
            .spearman(DEFAULT_SPEARMAN)
            .swordsman(DEFAULT_SWORDSMAN)
            .archers(DEFAULT_ARCHERS)
            .crossbowman(DEFAULT_CROSSBOWMAN);
        return humanUnits;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HumanUnits createUpdatedEntity() {
        HumanUnits humanUnits = new HumanUnits()
            .spearman(UPDATED_SPEARMAN)
            .swordsman(UPDATED_SWORDSMAN)
            .archers(UPDATED_ARCHERS)
            .crossbowman(UPDATED_CROSSBOWMAN);
        return humanUnits;
    }

    @BeforeEach
    public void initTest() {
        humanUnitsRepository.deleteAll();
        humanUnits = createEntity();
    }

    @Test
    public void createHumanUnits() throws Exception {
        int databaseSizeBeforeCreate = humanUnitsRepository.findAll().size();

        // Create the HumanUnits
        restHumanUnitsMockMvc.perform(post("/api/human-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(humanUnits)))
            .andExpect(status().isCreated());

        // Validate the HumanUnits in the database
        List<HumanUnits> humanUnitsList = humanUnitsRepository.findAll();
        assertThat(humanUnitsList).hasSize(databaseSizeBeforeCreate + 1);
        HumanUnits testHumanUnits = humanUnitsList.get(humanUnitsList.size() - 1);
        assertThat(testHumanUnits.getSpearman()).isEqualTo(DEFAULT_SPEARMAN);
        assertThat(testHumanUnits.getSwordsman()).isEqualTo(DEFAULT_SWORDSMAN);
        assertThat(testHumanUnits.getArchers()).isEqualTo(DEFAULT_ARCHERS);
        assertThat(testHumanUnits.getCrossbowman()).isEqualTo(DEFAULT_CROSSBOWMAN);
    }

    @Test
    public void createHumanUnitsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = humanUnitsRepository.findAll().size();

        // Create the HumanUnits with an existing ID
        humanUnits.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restHumanUnitsMockMvc.perform(post("/api/human-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(humanUnits)))
            .andExpect(status().isBadRequest());

        // Validate the HumanUnits in the database
        List<HumanUnits> humanUnitsList = humanUnitsRepository.findAll();
        assertThat(humanUnitsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllHumanUnits() throws Exception {
        // Initialize the database
        humanUnitsRepository.save(humanUnits);

        // Get all the humanUnitsList
        restHumanUnitsMockMvc.perform(get("/api/human-units?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(humanUnits.getId())))
            .andExpect(jsonPath("$.[*].spearman").value(hasItem(DEFAULT_SPEARMAN)))
            .andExpect(jsonPath("$.[*].swordsman").value(hasItem(DEFAULT_SWORDSMAN)))
            .andExpect(jsonPath("$.[*].archers").value(hasItem(DEFAULT_ARCHERS)))
            .andExpect(jsonPath("$.[*].crossbowman").value(hasItem(DEFAULT_CROSSBOWMAN)));
    }
    
    @Test
    public void getHumanUnits() throws Exception {
        // Initialize the database
        humanUnitsRepository.save(humanUnits);

        // Get the humanUnits
        restHumanUnitsMockMvc.perform(get("/api/human-units/{id}", humanUnits.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(humanUnits.getId()))
            .andExpect(jsonPath("$.spearman").value(DEFAULT_SPEARMAN))
            .andExpect(jsonPath("$.swordsman").value(DEFAULT_SWORDSMAN))
            .andExpect(jsonPath("$.archers").value(DEFAULT_ARCHERS))
            .andExpect(jsonPath("$.crossbowman").value(DEFAULT_CROSSBOWMAN));
    }

    @Test
    public void getNonExistingHumanUnits() throws Exception {
        // Get the humanUnits
        restHumanUnitsMockMvc.perform(get("/api/human-units/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateHumanUnits() throws Exception {
        // Initialize the database
        humanUnitsRepository.save(humanUnits);

        int databaseSizeBeforeUpdate = humanUnitsRepository.findAll().size();

        // Update the humanUnits
        HumanUnits updatedHumanUnits = humanUnitsRepository.findById(humanUnits.getId()).get();
        updatedHumanUnits
            .spearman(UPDATED_SPEARMAN)
            .swordsman(UPDATED_SWORDSMAN)
            .archers(UPDATED_ARCHERS)
            .crossbowman(UPDATED_CROSSBOWMAN);

        restHumanUnitsMockMvc.perform(put("/api/human-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedHumanUnits)))
            .andExpect(status().isOk());

        // Validate the HumanUnits in the database
        List<HumanUnits> humanUnitsList = humanUnitsRepository.findAll();
        assertThat(humanUnitsList).hasSize(databaseSizeBeforeUpdate);
        HumanUnits testHumanUnits = humanUnitsList.get(humanUnitsList.size() - 1);
        assertThat(testHumanUnits.getSpearman()).isEqualTo(UPDATED_SPEARMAN);
        assertThat(testHumanUnits.getSwordsman()).isEqualTo(UPDATED_SWORDSMAN);
        assertThat(testHumanUnits.getArchers()).isEqualTo(UPDATED_ARCHERS);
        assertThat(testHumanUnits.getCrossbowman()).isEqualTo(UPDATED_CROSSBOWMAN);
    }

    @Test
    public void updateNonExistingHumanUnits() throws Exception {
        int databaseSizeBeforeUpdate = humanUnitsRepository.findAll().size();

        // Create the HumanUnits

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHumanUnitsMockMvc.perform(put("/api/human-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(humanUnits)))
            .andExpect(status().isBadRequest());

        // Validate the HumanUnits in the database
        List<HumanUnits> humanUnitsList = humanUnitsRepository.findAll();
        assertThat(humanUnitsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteHumanUnits() throws Exception {
        // Initialize the database
        humanUnitsRepository.save(humanUnits);

        int databaseSizeBeforeDelete = humanUnitsRepository.findAll().size();

        // Delete the humanUnits
        restHumanUnitsMockMvc.perform(delete("/api/human-units/{id}", humanUnits.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<HumanUnits> humanUnitsList = humanUnitsRepository.findAll();
        assertThat(humanUnitsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HumanUnits.class);
        HumanUnits humanUnits1 = new HumanUnits();
        humanUnits1.setId("id1");
        HumanUnits humanUnits2 = new HumanUnits();
        humanUnits2.setId(humanUnits1.getId());
        assertThat(humanUnits1).isEqualTo(humanUnits2);
        humanUnits2.setId("id2");
        assertThat(humanUnits1).isNotEqualTo(humanUnits2);
        humanUnits1.setId(null);
        assertThat(humanUnits1).isNotEqualTo(humanUnits2);
    }
}
