package com.graphzero.messagebox.web.rest;

import com.graphzero.messagebox.MessageBoxApp;
import com.graphzero.messagebox.domain.PlayerWarDeclarationMessage;
import com.graphzero.messagebox.repository.PlayerWarDeclarationMessageRepository;
import com.graphzero.messagebox.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;


import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.graphzero.messagebox.web.rest.TestUtil.sameInstant;
import static com.graphzero.messagebox.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PlayerWarDeclarationMessageResource} REST controller.
 */
@EmbeddedKafka
@SpringBootTest(classes = MessageBoxApp.class)
public class PlayerWarDeclarationMessageResourceIT {

    private static final ZonedDateTime DEFAULT_DECLARATION_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DECLARATION_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DECLARATION_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    @Autowired
    private PlayerWarDeclarationMessageRepository playerWarDeclarationMessageRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    private MockMvc restPlayerWarDeclarationMessageMockMvc;

    private PlayerWarDeclarationMessage playerWarDeclarationMessage;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PlayerWarDeclarationMessageResource playerWarDeclarationMessageResource = new PlayerWarDeclarationMessageResource(playerWarDeclarationMessageRepository);
        this.restPlayerWarDeclarationMessageMockMvc = MockMvcBuilders.standaloneSetup(playerWarDeclarationMessageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PlayerWarDeclarationMessage createEntity() {
        PlayerWarDeclarationMessage playerWarDeclarationMessage = new PlayerWarDeclarationMessage()
            .declarationTime(DEFAULT_DECLARATION_TIME);
        return playerWarDeclarationMessage;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PlayerWarDeclarationMessage createUpdatedEntity() {
        PlayerWarDeclarationMessage playerWarDeclarationMessage = new PlayerWarDeclarationMessage()
            .declarationTime(UPDATED_DECLARATION_TIME);
        return playerWarDeclarationMessage;
    }

    @BeforeEach
    public void initTest() {
        playerWarDeclarationMessageRepository.deleteAll();
        playerWarDeclarationMessage = createEntity();
    }

    @Test
    public void createPlayerWarDeclarationMessage() throws Exception {
        int databaseSizeBeforeCreate = playerWarDeclarationMessageRepository.findAll().size();

        // Create the PlayerWarDeclarationMessage
        restPlayerWarDeclarationMessageMockMvc.perform(post("/api/player-war-declaration-messages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(playerWarDeclarationMessage)))
            .andExpect(status().isCreated());

        // Validate the PlayerWarDeclarationMessage in the database
        List<PlayerWarDeclarationMessage> playerWarDeclarationMessageList = playerWarDeclarationMessageRepository.findAll();
        assertThat(playerWarDeclarationMessageList).hasSize(databaseSizeBeforeCreate + 1);
        PlayerWarDeclarationMessage testPlayerWarDeclarationMessage = playerWarDeclarationMessageList.get(playerWarDeclarationMessageList.size() - 1);
        assertThat(testPlayerWarDeclarationMessage.getDeclarationTime()).isEqualTo(DEFAULT_DECLARATION_TIME);
    }

    @Test
    public void createPlayerWarDeclarationMessageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = playerWarDeclarationMessageRepository.findAll().size();

        // Create the PlayerWarDeclarationMessage with an existing ID
        playerWarDeclarationMessage.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restPlayerWarDeclarationMessageMockMvc.perform(post("/api/player-war-declaration-messages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(playerWarDeclarationMessage)))
            .andExpect(status().isBadRequest());

        // Validate the PlayerWarDeclarationMessage in the database
        List<PlayerWarDeclarationMessage> playerWarDeclarationMessageList = playerWarDeclarationMessageRepository.findAll();
        assertThat(playerWarDeclarationMessageList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllPlayerWarDeclarationMessages() throws Exception {
        // Initialize the database
        playerWarDeclarationMessageRepository.save(playerWarDeclarationMessage);

        // Get all the playerWarDeclarationMessageList
        restPlayerWarDeclarationMessageMockMvc.perform(get("/api/player-war-declaration-messages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(playerWarDeclarationMessage.getId())))
            .andExpect(jsonPath("$.[*].declarationTime").value(hasItem(sameInstant(DEFAULT_DECLARATION_TIME))));
    }
    
    @Test
    public void getPlayerWarDeclarationMessage() throws Exception {
        // Initialize the database
        playerWarDeclarationMessageRepository.save(playerWarDeclarationMessage);

        // Get the playerWarDeclarationMessage
        restPlayerWarDeclarationMessageMockMvc.perform(get("/api/player-war-declaration-messages/{id}", playerWarDeclarationMessage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(playerWarDeclarationMessage.getId()))
            .andExpect(jsonPath("$.declarationTime").value(sameInstant(DEFAULT_DECLARATION_TIME)));
    }

    @Test
    public void getNonExistingPlayerWarDeclarationMessage() throws Exception {
        // Get the playerWarDeclarationMessage
        restPlayerWarDeclarationMessageMockMvc.perform(get("/api/player-war-declaration-messages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updatePlayerWarDeclarationMessage() throws Exception {
        // Initialize the database
        playerWarDeclarationMessageRepository.save(playerWarDeclarationMessage);

        int databaseSizeBeforeUpdate = playerWarDeclarationMessageRepository.findAll().size();

        // Update the playerWarDeclarationMessage
        PlayerWarDeclarationMessage updatedPlayerWarDeclarationMessage = playerWarDeclarationMessageRepository.findById(playerWarDeclarationMessage.getId()).get();
        updatedPlayerWarDeclarationMessage
            .declarationTime(UPDATED_DECLARATION_TIME);

        restPlayerWarDeclarationMessageMockMvc.perform(put("/api/player-war-declaration-messages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPlayerWarDeclarationMessage)))
            .andExpect(status().isOk());

        // Validate the PlayerWarDeclarationMessage in the database
        List<PlayerWarDeclarationMessage> playerWarDeclarationMessageList = playerWarDeclarationMessageRepository.findAll();
        assertThat(playerWarDeclarationMessageList).hasSize(databaseSizeBeforeUpdate);
        PlayerWarDeclarationMessage testPlayerWarDeclarationMessage = playerWarDeclarationMessageList.get(playerWarDeclarationMessageList.size() - 1);
        assertThat(testPlayerWarDeclarationMessage.getDeclarationTime()).isEqualTo(UPDATED_DECLARATION_TIME);
    }

    @Test
    public void updateNonExistingPlayerWarDeclarationMessage() throws Exception {
        int databaseSizeBeforeUpdate = playerWarDeclarationMessageRepository.findAll().size();

        // Create the PlayerWarDeclarationMessage

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPlayerWarDeclarationMessageMockMvc.perform(put("/api/player-war-declaration-messages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(playerWarDeclarationMessage)))
            .andExpect(status().isBadRequest());

        // Validate the PlayerWarDeclarationMessage in the database
        List<PlayerWarDeclarationMessage> playerWarDeclarationMessageList = playerWarDeclarationMessageRepository.findAll();
        assertThat(playerWarDeclarationMessageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deletePlayerWarDeclarationMessage() throws Exception {
        // Initialize the database
        playerWarDeclarationMessageRepository.save(playerWarDeclarationMessage);

        int databaseSizeBeforeDelete = playerWarDeclarationMessageRepository.findAll().size();

        // Delete the playerWarDeclarationMessage
        restPlayerWarDeclarationMessageMockMvc.perform(delete("/api/player-war-declaration-messages/{id}", playerWarDeclarationMessage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PlayerWarDeclarationMessage> playerWarDeclarationMessageList = playerWarDeclarationMessageRepository.findAll();
        assertThat(playerWarDeclarationMessageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlayerWarDeclarationMessage.class);
        PlayerWarDeclarationMessage playerWarDeclarationMessage1 = new PlayerWarDeclarationMessage();
        playerWarDeclarationMessage1.setId("id1");
        PlayerWarDeclarationMessage playerWarDeclarationMessage2 = new PlayerWarDeclarationMessage();
        playerWarDeclarationMessage2.setId(playerWarDeclarationMessage1.getId());
        assertThat(playerWarDeclarationMessage1).isEqualTo(playerWarDeclarationMessage2);
        playerWarDeclarationMessage2.setId("id2");
        assertThat(playerWarDeclarationMessage1).isNotEqualTo(playerWarDeclarationMessage2);
        playerWarDeclarationMessage1.setId(null);
        assertThat(playerWarDeclarationMessage1).isNotEqualTo(playerWarDeclarationMessage2);
    }
}
