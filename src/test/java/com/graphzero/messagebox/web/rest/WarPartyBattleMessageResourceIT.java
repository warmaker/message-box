package com.graphzero.messagebox.web.rest;

import com.graphzero.messagebox.MessageBoxApp;
import com.graphzero.messagebox.domain.WarPartyBattleMessage;
import com.graphzero.messagebox.repository.WarPartyBattleMessageRepository;
import com.graphzero.messagebox.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;


import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.graphzero.messagebox.web.rest.TestUtil.sameInstant;
import static com.graphzero.messagebox.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link WarPartyBattleMessageResource} REST controller.
 */
@EmbeddedKafka
@SpringBootTest(classes = MessageBoxApp.class)
public class WarPartyBattleMessageResourceIT {

    private static final ZonedDateTime DEFAULT_BATTLE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_BATTLE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_BATTLE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final Long DEFAULT_ATTACKER_WAR_PARTY_ID = 1L;
    private static final Long UPDATED_ATTACKER_WAR_PARTY_ID = 2L;
    private static final Long SMALLER_ATTACKER_WAR_PARTY_ID = 1L - 1L;

    private static final Long DEFAULT_DEFENDER_WAR_PARTY_ID = 1L;
    private static final Long UPDATED_DEFENDER_WAR_PARTY_ID = 2L;
    private static final Long SMALLER_DEFENDER_WAR_PARTY_ID = 1L - 1L;

    private static final Long DEFAULT_ATTACKER_ID = 1L;
    private static final Long UPDATED_ATTACKER_ID = 2L;
    private static final Long SMALLER_ATTACKER_ID = 1L - 1L;

    private static final Long DEFAULT_DEFENDER_ID = 1L;
    private static final Long UPDATED_DEFENDER_ID = 2L;
    private static final Long SMALLER_DEFENDER_ID = 1L - 1L;

    @Autowired
    private WarPartyBattleMessageRepository warPartyBattleMessageRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    private MockMvc restWarPartyBattleMessageMockMvc;

    private WarPartyBattleMessage warPartyBattleMessage;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final WarPartyBattleMessageResource warPartyBattleMessageResource = new WarPartyBattleMessageResource(warPartyBattleMessageRepository);
        this.restWarPartyBattleMessageMockMvc = MockMvcBuilders.standaloneSetup(warPartyBattleMessageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WarPartyBattleMessage createEntity() {
        WarPartyBattleMessage warPartyBattleMessage = new WarPartyBattleMessage()
            .battleTime(DEFAULT_BATTLE_TIME)
            .attackerWarPartyId(DEFAULT_ATTACKER_WAR_PARTY_ID)
            .defenderWarPartyId(DEFAULT_DEFENDER_WAR_PARTY_ID)
            .attackerId(DEFAULT_ATTACKER_ID)
            .defenderId(DEFAULT_DEFENDER_ID);
        return warPartyBattleMessage;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WarPartyBattleMessage createUpdatedEntity() {
        WarPartyBattleMessage warPartyBattleMessage = new WarPartyBattleMessage()
            .battleTime(UPDATED_BATTLE_TIME)
            .attackerWarPartyId(UPDATED_ATTACKER_WAR_PARTY_ID)
            .defenderWarPartyId(UPDATED_DEFENDER_WAR_PARTY_ID)
            .attackerId(UPDATED_ATTACKER_ID)
            .defenderId(UPDATED_DEFENDER_ID);
        return warPartyBattleMessage;
    }

    @BeforeEach
    public void initTest() {
        warPartyBattleMessageRepository.deleteAll();
        warPartyBattleMessage = createEntity();
    }

    @Test
    public void createWarPartyBattleMessage() throws Exception {
        int databaseSizeBeforeCreate = warPartyBattleMessageRepository.findAll().size();

        // Create the WarPartyBattleMessage
        restWarPartyBattleMessageMockMvc.perform(post("/api/war-party-battle-messages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(warPartyBattleMessage)))
            .andExpect(status().isCreated());

        // Validate the WarPartyBattleMessage in the database
        List<WarPartyBattleMessage> warPartyBattleMessageList = warPartyBattleMessageRepository.findAll();
        assertThat(warPartyBattleMessageList).hasSize(databaseSizeBeforeCreate + 1);
        WarPartyBattleMessage testWarPartyBattleMessage = warPartyBattleMessageList.get(warPartyBattleMessageList.size() - 1);
        assertThat(testWarPartyBattleMessage.getBattleTime()).isEqualTo(DEFAULT_BATTLE_TIME);
        assertThat(testWarPartyBattleMessage.getAttackerWarPartyId()).isEqualTo(DEFAULT_ATTACKER_WAR_PARTY_ID);
        assertThat(testWarPartyBattleMessage.getDefenderWarPartyId()).isEqualTo(DEFAULT_DEFENDER_WAR_PARTY_ID);
        assertThat(testWarPartyBattleMessage.getAttackerId()).isEqualTo(DEFAULT_ATTACKER_ID);
        assertThat(testWarPartyBattleMessage.getDefenderId()).isEqualTo(DEFAULT_DEFENDER_ID);
    }

    @Test
    public void createWarPartyBattleMessageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = warPartyBattleMessageRepository.findAll().size();

        // Create the WarPartyBattleMessage with an existing ID
        warPartyBattleMessage.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restWarPartyBattleMessageMockMvc.perform(post("/api/war-party-battle-messages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(warPartyBattleMessage)))
            .andExpect(status().isBadRequest());

        // Validate the WarPartyBattleMessage in the database
        List<WarPartyBattleMessage> warPartyBattleMessageList = warPartyBattleMessageRepository.findAll();
        assertThat(warPartyBattleMessageList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllWarPartyBattleMessages() throws Exception {
        // Initialize the database
        warPartyBattleMessageRepository.save(warPartyBattleMessage);

        // Get all the warPartyBattleMessageList
        restWarPartyBattleMessageMockMvc.perform(get("/api/war-party-battle-messages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(warPartyBattleMessage.getId())))
            .andExpect(jsonPath("$.[*].battleTime").value(hasItem(sameInstant(DEFAULT_BATTLE_TIME))))
            .andExpect(jsonPath("$.[*].attackerWarPartyId").value(hasItem(DEFAULT_ATTACKER_WAR_PARTY_ID.intValue())))
            .andExpect(jsonPath("$.[*].defenderWarPartyId").value(hasItem(DEFAULT_DEFENDER_WAR_PARTY_ID.intValue())))
            .andExpect(jsonPath("$.[*].attackerId").value(hasItem(DEFAULT_ATTACKER_ID.intValue())))
            .andExpect(jsonPath("$.[*].defenderId").value(hasItem(DEFAULT_DEFENDER_ID.intValue())));
    }
    
    @Test
    public void getWarPartyBattleMessage() throws Exception {
        // Initialize the database
        warPartyBattleMessageRepository.save(warPartyBattleMessage);

        // Get the warPartyBattleMessage
        restWarPartyBattleMessageMockMvc.perform(get("/api/war-party-battle-messages/{id}", warPartyBattleMessage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(warPartyBattleMessage.getId()))
            .andExpect(jsonPath("$.battleTime").value(sameInstant(DEFAULT_BATTLE_TIME)))
            .andExpect(jsonPath("$.attackerWarPartyId").value(DEFAULT_ATTACKER_WAR_PARTY_ID.intValue()))
            .andExpect(jsonPath("$.defenderWarPartyId").value(DEFAULT_DEFENDER_WAR_PARTY_ID.intValue()))
            .andExpect(jsonPath("$.attackerId").value(DEFAULT_ATTACKER_ID.intValue()))
            .andExpect(jsonPath("$.defenderId").value(DEFAULT_DEFENDER_ID.intValue()));
    }

    @Test
    public void getNonExistingWarPartyBattleMessage() throws Exception {
        // Get the warPartyBattleMessage
        restWarPartyBattleMessageMockMvc.perform(get("/api/war-party-battle-messages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateWarPartyBattleMessage() throws Exception {
        // Initialize the database
        warPartyBattleMessageRepository.save(warPartyBattleMessage);

        int databaseSizeBeforeUpdate = warPartyBattleMessageRepository.findAll().size();

        // Update the warPartyBattleMessage
        WarPartyBattleMessage updatedWarPartyBattleMessage = warPartyBattleMessageRepository.findById(warPartyBattleMessage.getId()).get();
        updatedWarPartyBattleMessage
            .battleTime(UPDATED_BATTLE_TIME)
            .attackerWarPartyId(UPDATED_ATTACKER_WAR_PARTY_ID)
            .defenderWarPartyId(UPDATED_DEFENDER_WAR_PARTY_ID)
            .attackerId(UPDATED_ATTACKER_ID)
            .defenderId(UPDATED_DEFENDER_ID);

        restWarPartyBattleMessageMockMvc.perform(put("/api/war-party-battle-messages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedWarPartyBattleMessage)))
            .andExpect(status().isOk());

        // Validate the WarPartyBattleMessage in the database
        List<WarPartyBattleMessage> warPartyBattleMessageList = warPartyBattleMessageRepository.findAll();
        assertThat(warPartyBattleMessageList).hasSize(databaseSizeBeforeUpdate);
        WarPartyBattleMessage testWarPartyBattleMessage = warPartyBattleMessageList.get(warPartyBattleMessageList.size() - 1);
        assertThat(testWarPartyBattleMessage.getBattleTime()).isEqualTo(UPDATED_BATTLE_TIME);
        assertThat(testWarPartyBattleMessage.getAttackerWarPartyId()).isEqualTo(UPDATED_ATTACKER_WAR_PARTY_ID);
        assertThat(testWarPartyBattleMessage.getDefenderWarPartyId()).isEqualTo(UPDATED_DEFENDER_WAR_PARTY_ID);
        assertThat(testWarPartyBattleMessage.getAttackerId()).isEqualTo(UPDATED_ATTACKER_ID);
        assertThat(testWarPartyBattleMessage.getDefenderId()).isEqualTo(UPDATED_DEFENDER_ID);
    }

    @Test
    public void updateNonExistingWarPartyBattleMessage() throws Exception {
        int databaseSizeBeforeUpdate = warPartyBattleMessageRepository.findAll().size();

        // Create the WarPartyBattleMessage

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWarPartyBattleMessageMockMvc.perform(put("/api/war-party-battle-messages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(warPartyBattleMessage)))
            .andExpect(status().isBadRequest());

        // Validate the WarPartyBattleMessage in the database
        List<WarPartyBattleMessage> warPartyBattleMessageList = warPartyBattleMessageRepository.findAll();
        assertThat(warPartyBattleMessageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteWarPartyBattleMessage() throws Exception {
        // Initialize the database
        warPartyBattleMessageRepository.save(warPartyBattleMessage);

        int databaseSizeBeforeDelete = warPartyBattleMessageRepository.findAll().size();

        // Delete the warPartyBattleMessage
        restWarPartyBattleMessageMockMvc.perform(delete("/api/war-party-battle-messages/{id}", warPartyBattleMessage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<WarPartyBattleMessage> warPartyBattleMessageList = warPartyBattleMessageRepository.findAll();
        assertThat(warPartyBattleMessageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(WarPartyBattleMessage.class);
        WarPartyBattleMessage warPartyBattleMessage1 = new WarPartyBattleMessage();
        warPartyBattleMessage1.setId("id1");
        WarPartyBattleMessage warPartyBattleMessage2 = new WarPartyBattleMessage();
        warPartyBattleMessage2.setId(warPartyBattleMessage1.getId());
        assertThat(warPartyBattleMessage1).isEqualTo(warPartyBattleMessage2);
        warPartyBattleMessage2.setId("id2");
        assertThat(warPartyBattleMessage1).isNotEqualTo(warPartyBattleMessage2);
        warPartyBattleMessage1.setId(null);
        assertThat(warPartyBattleMessage1).isNotEqualTo(warPartyBattleMessage2);
    }
}
